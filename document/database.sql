-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 26, 2020 at 05:44 PM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `chat`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat_admin`
--

CREATE TABLE `chat_admin` (
  `id` int(10) NOT NULL,
  `admin` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `create_at` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `chat_friends`
--

CREATE TABLE `chat_friends` (
  `id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `create_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `chat_m_config`
--

CREATE TABLE `chat_m_config` (
  `id` int(10) NOT NULL COMMENT 'ID',
  `code` varchar(255) DEFAULT NULL COMMENT '配置名称',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `group_id` varchar(50) DEFAULT NULL COMMENT '分组名称',
  `val` text COMMENT '配置值',
  `type` tinyint(1) NOT NULL COMMENT '类型',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `value_scope` varchar(50) DEFAULT NULL COMMENT '值的范围',
  `title_scope` varchar(255) DEFAULT NULL COMMENT '对应value_scope的中文解释',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统手机端配置信息表';

-- --------------------------------------------------------

--
-- Table structure for table `chat_offline`
--

CREATE TABLE `chat_offline` (
  `id` bigint(20) NOT NULL,
  `from_id` varchar(50) DEFAULT NULL COMMENT '离线发送方',
  `to_id` varchar(50) DEFAULT NULL COMMENT '离线接收方',
  `content` varchar(1000) DEFAULT NULL COMMENT '发送的离线内容',
  `status` tinyint(4) DEFAULT '0' COMMENT '发送状态：0-未发送,1-已发送',
  `addtime` int(11) DEFAULT NULL COMMENT '发送方发送时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `chat_user`
--

CREATE TABLE `chat_user` (
  `id` bigint(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nike_name` varchar(30) NOT NULL,
  `head_image` varchar(200) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `create_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat_admin`
--
ALTER TABLE `chat_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_friends`
--
ALTER TABLE `chat_friends`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `chat_m_config`
--
ALTER TABLE `chat_m_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`) USING BTREE;

--
-- Indexes for table `chat_offline`
--
ALTER TABLE `chat_offline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_user`
--
ALTER TABLE `chat_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat_admin`
--
ALTER TABLE `chat_admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_friends`
--
ALTER TABLE `chat_friends`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_m_config`
--
ALTER TABLE `chat_m_config`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- AUTO_INCREMENT for table `chat_offline`
--
ALTER TABLE `chat_offline`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_user`
--
ALTER TABLE `chat_user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
