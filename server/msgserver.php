<?php
date_default_timezone_set("Asia/Shanghai");
$globals_data = require_once '../public/db_config.php';
$globals_redis = require_once '../public/directory_init.php';
$globals_chat = require_once './config.php';
$server = new swoole_websocket_server("0.0.0.0", 9051);
$server->set([
    'worker_num' => 2,
    'heartbeat_check_interval' => 60,
    'heartbeat_idle_time' => 300,
]);
$redis = new Redis();
$redis->connect('127.0.0.1', 6379);
$redis->select(2);
define('DB_PREFIX',$globals_data['DB_PREFIX']);
$db = new mysqli( $globals_data['DB_HOST'],  $globals_data['DB_USER'],  $globals_data['DB_PWD'], $globals_data['DB_NAME']);
if (!$db)
{
    echo("连接错误: " . mysqli_connect_error());
}
$db -> set_charset('utf8');
$GLOBALS['db']=$db;
$server->on('open', function (swoole_websocket_server $server, $request) {
    echo "server: handshake success with fd{$request->fd}\n";

});
$server->on('message', function (swoole_websocket_server $server, $from) {
    $data = json_decode($from->data,true);
    print_r($data);
    if($data['flag'] == 'chatlist'){
        get_friend_msg_list($server,$from);
    }

});


function get_friend_msg_list($server,$from){
    //处理发给该用户的离线消息
    $sql="SELECT DISTINCT from_id,to_id FROM ".DB_PREFIX."chat_offline  WHERE status=0 ";
    print_r($sql);
    echo "\n\r";
    $res = array();
    if ($result = $GLOBALS['db']->query($sql)) {

        while ($row = $result->fetch_assoc()) {
            array_push($res, $row);
        }
        $result->free();
    }

    print_r($res);
    echo "\n\r";
    if($res){
        $return=array();
        foreach ($res as $item){

            $sql = "SELECT c.from_id as list_from_id,to_id as user_id,count(*) as total,".
                "content as msg ,u.head_image,u.nick_name as nick_name,c.addtime   FROM "
                .DB_PREFIX."chat_offline as c,  ".DB_PREFIX."user as u WHERE  u.id=c.from_id and "
                . "c.status='0' and  c.from_id=".$item['from_id'] ." and c.to_id=".$item['to_id']." order by c.id desc" ;
            if ($result = $GLOBALS['db']->query($sql)) {
                while ($row = $result->fetch_assoc()) {
                    array_push($return, $row);
                }
                $result->free();
            }
        }
        if($return){
            foreach ($return as &$v){
                $v['addtime']=date('Y-m-d H:i:s',  $v['addtime']);
            }
            $re = json_encode(array('list'=>$return));
            $server->push($from->fd , $re);
        }
    }

}
$server->on('close', function ($ser, $fd) {




    echo "client {$fd} closed\n";
});
$server->start();
