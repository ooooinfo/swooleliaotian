<?php
date_default_timezone_set("Asia/Shanghai");
$globals_data = require_once './db_config.php';
$globals_redis = require_once './directory_init.php';
$globals_chat = require_once './config.php';
$server = new swoole_websocket_server("0.0.0.0", 9052);
$server->set([
    'worker_num' => 4,
    'heartbeat_check_interval' => 60,
    'heartbeat_idle_time' => 100,
]);

$redis = new Redis();
$redis->connect('127.0.0.1', 6379);
$redis->select(2);

define('DB_PREFIX',$globals_data['DB_PREFIX']);
$db = new mysqli( $globals_data['DB_HOST'],  $globals_data['DB_USER'],  $globals_data['DB_PWD'], $globals_data['DB_NAME']);
if (!$db)
{
    echo("连接错误: " . mysqli_connect_error());
}
$db -> set_charset('utf8');
$GLOBALS['db']=$db;
$server->on('open', function (swoole_websocket_server $server, $request) {
    echo "server: handshake success with fd{$request->fd}\n";
    //$request->fd 是客户端id
});
$server->on('message', function (swoole_websocket_server $server, $from) {
    $data = json_decode($from->data,true);
    if($data['flag'] == 'init'){
        $GLOBALS['redis']->set($data['from'], $from->fd);
        chat_online($server,$data,$from);
    }else if($data['flag'] == 'msg'){
        chat($server,$data);
    }
    else if($data['flag'] == 'group'){
        //todo 群聊
    }else if($data['flag'] == 'all'){
        //全站广播
        foreach($server->connections as $fd){
            echo "全站广播:".$fd."-".$data."\n\r";
            $server->push($fd , $data);
        }
    }
});


function chat_online($server,$data,$from){
    //用户刚连接的时候初始化，每个用户登录时记录该用户对应的fd
    //处理发给该用户的离线消息
    $sql = "SELECT * FROM ".DB_PREFIX."chat_offline WHERE `to_id`='{$data['from']}' AND `from_id`='{$data['to']}' AND `status`='0' ORDER BY addtime ASC;";
    if ($result = $GLOBALS['db']->query($sql)) {
        $re = array();
        while ($row = $result->fetch_assoc()) {
            array_push($re, $row);
        }
        $result->free();
        foreach($re as $content){
            //$newid=$content['to_id'];
            //$content = json_encode($content);

            print_r($from->fd);
            $tmp['to'] = $content['to_id']; //消息来自于谁
            $tmp['from'] = $content['from_id']; //消息来自于谁
            $tmp['content']  = "<span  style='size: 9px ;color: grey;' >离线信息:</span>".$content['content']."<br><span  style='size: 9px ;color: grey;' >".date('Y-m-d H:i:s',  $content['addtime'])."</span>";  //消息内容
            $re = json_encode($tmp);
            $server->push($from->fd , $re);
        }
        //设置消息池中的消息为已发送
        $sql = "UPDATE ".DB_PREFIX."chat_offline SET `status`=1 WHERE `to_id`='{$data['from']}' AND `from_id`='{$data['to']}';";
        $GLOBALS['db']->query($sql);
    }
}

function chat($server,$data){
    //非初始化的信息发送，一对一聊天，根据每个用户对应的fd发给特定用户
    $tofd = $GLOBALS['redis']->get($data['to']); //消息要发给谁
    $fds = []; //所有在线的用户(打开聊天窗口的用户)
    foreach($server->connections as $fd){
        array_push($fds, $fd);
    }
    if(in_array($tofd,$fds)){
        $tmp['from'] = $data['from']; //消息来自于谁
        $tmp['to'] = $data['to']; //消息来自于谁
        $tmp['content']  = $data['content']."<br><span  style='size: 10 ;color: grey;' >".date('Y-m-d H:i:s',  time())."</span>"; //消息内容
        $re = json_encode($tmp);
        $server->push($tofd , $re);
    }else{
        offLine($data);
    }
}

function offLine($data){
    $time = time();
    $sql = "INSERT INTO ".DB_PREFIX."chat_offline (`to_id`,`from_id`,`content`,`status`,`addtime`) VALUES ('{$data['to']}','{$data['from']}','{$data['content']}','0','{$time}');";
    $GLOBALS['db']->query($sql);
}

$server->on('close', function ($data, $fd) {
    $arr=$GLOBALS['redis']->keys('*'); //消息要发给谁);

   print_r($arr);
    foreach ($arr as $key=>$v){
        $nval= $GLOBALS['redis']->get($v);
       // echo "\n\r";
        if($nval==$fd){

            echo "key:".$v;
            echo "\n\r";
            $GLOBALS['redis']->del($v);
        }
    }


    echo "client {$fd} closed\n";
});
$server->start();



